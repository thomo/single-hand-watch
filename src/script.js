function initGadget() {
	System.Gadget.visibilityChanged = visibilityChanged;
}

function updateHand() {
	var date = new Date(System.Time.getLocalTime(System.Time.currentTimeZone));
	var hour   = date.getHours();
	var minute = date.getMinutes();
	
	hand.Rotation = (hour-12) * 15 + minute * 0.25 ;
	
	var d = new Date();
	var timeout = ((60 - d.getSeconds()) * 1000) - d.getMilliseconds();
	
	if (timeout > 0) {
		setTimeout("updateHand()", timeout);
	} else {
		updateHand();
	}
}

function visibilityChanged(event) {
	if (System.Gadget.visible) {
			updateHand();
	}
}
